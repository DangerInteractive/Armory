# Armory

**a fast pool for storing lots of objects of one type without fragmenting**

## How to Use

This is a Rust library, available through crates.io.
To install in your project, add it to your Cargo.toml as a dependency:

```toml
[dependencies]
armory = "0.1.0"
```

Or run the following command from within your project directory:

```shell
cargo add armory
```
