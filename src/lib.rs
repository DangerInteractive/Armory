//! a data structure that enables quick random access/insertion/removal of
//! unordered items without reallocating or shifting elements

use error::{DeleteError, PutError};

mod array_depot;
pub mod error;
pub mod link;
pub mod space;
mod vec_depot;

pub use array_depot::ArrayDepot;
pub use vec_depot::VecDepot;

/// capability to store values in a depot data structure implementation
pub trait Depot<T> {
    /// get a value from the depot given its index
    fn get(&self, index: usize) -> Option<&T>;

    /// store a value in the depot, returning its assigned index
    fn put(&mut self, value: T) -> Result<usize, PutError>;

    /// delete a value from the depot given its index
    fn delete(&mut self, index: usize) -> Result<(), DeleteError>;
}
