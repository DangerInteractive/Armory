//! code for storing individual memory cells in a depot data structure

use crate::link::Link;
use crate::space::Space::{Empty, Full};
use std::panic::panic_any;
use std::mem::replace;

/// a single memory cell in a depot, containing either a value, or a link
/// to another empty memory cell if it's empty (and another empty `Space`
/// existed when it was created)
#[derive(Clone, Copy, Debug)]
pub enum Space<T> {
    Full(T),
    Empty(Link),
}

impl<T> Space<T> {
    /// return the `Link` if space is empty, panic otherwise
    #[must_use]
    pub fn expect_empty(self, msg: &'static str) -> Link {
        match self {
            Empty(link) => link,
            Full(_) => panic_any(msg),
        }
    }

    /// return the value if space is full, panic otherwise
    #[must_use]
    pub fn expect_full(self, msg: &'static str) -> T {
        match self {
            Full(value) => value,
            Empty(_) => panic_any(msg),
        }
    }

    /// return an immutable reference to the `Link` if space is empty,
    /// panic otherwise
    #[must_use]
    pub fn expect_empty_ref(&self, msg: &'static str) -> &Link {
        match self {
            Empty(link) => link,
            Full(_) => panic_any(msg),
        }
    }

    /// return an immutable reference to the value if space is full,
    /// panic otherwise
    #[must_use]
    pub fn expect_full_ref(&self, msg: &'static str) -> &T {
        match self {
            Full(value) => value,
            Empty(_) => panic_any(msg),
        }
    }

    /// return a mutable reference to the `Link` if space is empty,
    /// panic otherwise
    #[must_use]
    pub fn expect_empty_mut(&mut self, msg: &'static str) -> &mut Link {
        match self {
            Empty(link) => link,
            Full(_) => panic_any(msg),
        }
    }

    /// return a mutable reference to the value if space is empty,
    /// panic otherwise
    #[must_use]
    pub fn expect_full_mut(&mut self, msg: &'static str) -> &mut T {
        match self {
            Full(value) => value,
            Empty(_) => panic_any(msg),
        }
    }

    /// return true if space is empty, false otherwise
    #[must_use]
    pub fn is_empty(&self) -> bool {
        matches!(self, Empty(_))
    }

    /// return true if space is full, false otherwise
    #[must_use]
    pub fn is_full(&self) -> bool {
        matches!(self, Full(_))
    }
}

/// depot internals: ut a value into a space and update the next empty link
pub fn put_space<T>(space: &mut Space<T>, next_empty: &mut Link, value: T) {
    *next_empty =
        replace(space, Full(value)).expect_empty("next_empty pointed to a non-empty space");
}

/// depot internals: delete a value from a space and update the next empty link
pub fn delete_space<T>(space: &mut Space<T>, next_empty: &mut Link, index: usize) {
    if space.is_full() {
        let link = Link::push_new(index, next_empty);
        *space = Empty(link);
    }
}
