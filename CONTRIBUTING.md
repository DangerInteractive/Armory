# Contributing

Thanks for contributing!

This document contains a collection of guidelines and rules concerning the
development and contribution to armory.
If you have never contributed before, please make sure to read through this
guide to be sure you understand the rules.

For those who regularly contribute, or who have contributed in the past, please
note that these guidelines and rules may change, so keep an eye out for updates
to this file and review any changes.

## Table of Contents
- [Code of Conduct](#code-of-conduct)
- [Developer Certificate of Origin](#developer-certificate-of-origin)
- [Style Guide](#style-guide)

## Code of Conduct

This project and everyone participating in it is governed by the
[Code of Conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

Please report unacceptable behavior to
[alex@dangerzonegames.com](mailto:alex@dangerzonegames.com).

## Developer Certificate of Origin

All contributions to this project (meaning every commit) must include an
agreement to the [Developer Certificate of Origin](DCO.txt). Make sure to read
it in full before agreeing.

To indicate agreement, add a sign-off to your commit by using git's `-s` flag.

The sign-off must use your legal name, not an alias.

## Style Guide

All code is expected to be formatted by rustfmt.

We recommend configuring your IDE to automatically format code using rustfmt if
that feature is available to you.

Additionally, code should follow all recommendations in the official
[Rust API Guidelines](https://rust-lang.github.io/api-guidelines/).

Please take the time to review that document if you haven't already, and please
do your due diligence in reviewing your own code against these standards before
opening a pull request.
